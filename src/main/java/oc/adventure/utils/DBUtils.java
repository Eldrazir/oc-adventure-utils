package oc.adventure.utils;

public class DBUtils {

    public static final String EXTERNAL_ID_DB = "external_id";
    public static final String EXTERNAL_ID ="externalId";

    public static final String WHERE = "WHERE";
    public static final String OR = "OR";
    public static final String AND = "AND";
    public static final String ORDER_BY = "ORDER BY";
    public static final String DESC = "DESC";
    public static final String ASC = "ASC";
    public static final String IN = "IN";
    public static final String MEMBER_OF = "MEMBER OF";
    public static final String NAME = "name";
    public static final String ID = "id";
    public static final String ROLE = "role";
    public static final String USER = "user";
    public static final String MAIL = "mail";  
    public static final String ADVENTURE = "adventure";
    public static final String CATEGORY = "category";
    public static final String SESSION = "session";
    public static final String COMMENT ="comment";
  
    public static final String FIRST_NAME_DB = "first_name";
    public static final String LAST_NAME_DB = "last_name";
    public static final String PHONE_NUMBER_DB = "phone_number";
    public static final String PASSWORD_DB = "password";

    public static final String ADDRESS_DB = "address";
    public static final String CITY_DB = "city";
    public static final String REGION_BD = "region";
    public static final String POSTAL_CODE_DB = "postal_code";
    public static final String COUNTRY_DB = "country";

    public static final String USER_STANDARD = "USER_STANDARD";
    public static final String USER_ID = "user_id";
    public static final String TYPE_ADDRESS_ID = "type_address_id";
    public static final String ADVENTURE_ID = "adventure_id";
    public static final String NAME_ADVENTURE_DB = "name_adventure";
    public static final String LOCATION_ADVENTURE_DB = "location_adventure";
    public static final String DESCRIPTION_ADVENTURE_DB = "description_adventure";
    public static final String TOTAL_VOTERS_DB = "total_voter";
    public static final String TOTAL_VOTING_DB = "total_voting";
    public static final String ADVENTURE_RATING_DB="adventure_rating";
    public static final String TYPE_ADVENTURE_DB="type_adventure";

    public static final String START_DATE_DB = "start_date";
    public static final String END_DATE_DB = "end_date";
    public static final String LIMITE_BOOKING_DATE_DB = "limite_booking_date";
    public static final String PRICE_DB = "price";
    public static final String TOTAL_AVAILABLE_PLACE_DB = "total_available_place";
    public static final String DESCRIPTION_DB = "description";
    public static final String ADVENTURE_ID_DB = "adventure_id";

    public static final String LIMIT_BOOKING_DATE_DB = "limit_booking_date";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String IS_VALID = "is_valid";
    public static final String VALIDATION_TOKEN = "validation_token";
    public static final String TOTAL_COST_DB = "total_cost";
    public static final String STRIPE_EMAIL_DB = "stripe_email";
    public static final String CURRENCY = "currency";
    public static final String FULL_ADDRESS_DB = "full_address";
    public static final String STRIPE_TOKEN_DB = "stripe_token";
    public static final String PAIEMENT_DATE = "paiement_date";

    public static final String COMMENT_INPUT_DB = "comment_input";
    public static final String DATE_COMMENT_DB = "date_comment";

}
