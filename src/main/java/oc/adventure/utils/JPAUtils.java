package oc.adventure.utils;

import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Random;

public class JPAUtils
{
    public static <T> T getSingleResult(TypedQuery<T> query)
    {
        List<T> results = query.getResultList();
        if (results.size() == 1)
            return results.get(0);

        if (results.isEmpty())
            return null;

        throw new NonUniqueResultException();
    }

    public static String buildDynamicParameterQuery(String base){
        String query = base;


        return query;
    }

    private static char[] _base62chars =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
                    .toCharArray();

    private static Random _random = new Random();

    public static String GetBase62(int length) // Alternative à UUID
    {
        StringBuilder sb = new StringBuilder(length);

        for (int i=0; i<length; i++)
            sb.append(_base62chars[_random.nextInt(62)]);

        return sb.toString();
    }
}

